var window, document, alert, ro, p, co, e, ship, bullet;

var game_height = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
var gc = document.getElementById("game_container");

var smin = 25;
var smax = 100;

var rdspeed = Math.random() + 2;

var cmin = 0;
var cmax = 256;

var hmin = 5;
var hmax = 85;

var imin = 1000;
var imax = 9000;

var shmin = 1000000;
var shmax = 9999999;

var bid = [];
var sid = [];

function rand(min, max) { // min and max included 
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function create_ship() {
    var vessel = {
        
    };
    
    ship = document.createElement("div");
    ship.style.width = "65px";
    ship.style.height = "65px";
    ship.style.backgroundColor = "black";
    ship.className = "ship";
    ship.setAttribute("id", "ship");
    gc.appendChild(ship);   
    
}
create_ship();

function create_shot() { 
    
    var shot = {
        id: rand(shmin, shmax)
    };
    
    bullet = document.createElement("div");
    bullet.style.height = "45px";
    bullet.style.width = "2px";
    bullet.style.backgroundColor = "red";
    bullet.className = "bullet";
    bullet.setAttribute("id", shot.id);
    sid.push(shot.id);
    gc.appendChild(bullet);
    
    var c_shot = document.getElementById(shot.id);
    var sx = 0;
    
    var shot_speed = setInterval(function() {
        c_shot.style.bottom = sx + "%";
        sx += 8;//8 run //0.1 test
        
        if(sx > game_height) {
            clearInterval(shot_speed);
            c_shot.remove();
            
            for(var s = 0; s < sid.length; s++) {
                if(sid[s] == c_shot.id) {
                    sid.splice(s, 1);
                }
            }
        }
    }, 2);
    return sid;
}

var keyState = {};

window.addEventListener('keydown',function(e){
    keyState[e.keyCode || e.which] = true;
},true);

window.addEventListener('keyup',function(e){
    keyState[e.keyCode || e.which] = false;
},true);

var x = 50;

function gameLoop() {
    if (keyState[37] || keyState[65]){
        x -= 1;
    }
    if (keyState[39] || keyState[68]){
        x += 1;
    }
    if(keyState[83] || keyState[32]) {
        create_shot();
    }
    if(x >= 100) {
        x = 100;
    } else if (x <= 0) {
        x = 0;
    } else {
        x += 0;
    }
    ship.style.left = x + "%";
    if(bullet) {
        bullet.style.left = x + "%";
    }
    setTimeout(gameLoop, 10);
}
gameLoop();

function get_points(bs) {
    var points = Math.floor((100 - bs) / 3 + 5);
    return points;
}

function create_box() {
    
    var box = {
        size: rand(smin, smax),
        color: "rgb(" + rand(cmin, cmax) + ", " + rand(cmin, cmax) + ", " + rand(cmin, cmax) + ")",
        speed: rdspeed + "px",//working on speed now...
        rotation: ro,
        contain: co,
        element: e,
        hpos: rand(hmin, hmax) + "vw",
        id: rand(imin, imax)
    };

    var boxes = document.createElement("div");
    boxes.style.width = box.size + "px";
    boxes.style.height = box.size + "px";
    boxes.style.backgroundColor = box.color;
    boxes.style.left = box.hpos;
    boxes.className = "box";
    boxes.setAttribute("id", box.id);
    boxes.setAttribute("points", get_points(box.size));
    bid.push(box.id);
    gc.appendChild(boxes);
    
    var c_box = document.getElementById(box.id);
    var vpos = 5;
    var box_int = setInterval(function() {
        c_box.style.top = vpos + "px";
        vpos = vpos + 1.2;//1.2 run //0.2 test

        if(vpos > game_height) {
            clearInterval(box_int);
            c_box.remove();
            
            for(var i = 0; i < bid.length; i++) {
                if(bid[i] == c_box.id) {
                    bid.splice(i, 1);
                }
            }
        }
    }, 1);
}

setInterval(function() {
    create_box();
}, 2000);

var current_points = 0;

setInterval(function() {
    for(var t = 0; t < bid.length; t++) {
        var t_bid = document.getElementById(bid[t]);
        var t_pos = t_bid.getBoundingClientRect();
        var t_top = t_pos.top;
        var t_bot = t_pos.bottom;
        var t_left = t_pos.left;
        var t_right = t_pos.right;
        
        for(var b = 0; b < sid.length; b++) {
            var b_sid = document.getElementById(sid[b]);
            var b_pos = b_sid.getBoundingClientRect();
            var b_top = b_pos.top;
            var b_bot = b_pos.bottom;
            var b_left = b_pos.left;
            var b_right = b_pos.right;
            
            if(t_top < b_bot && t_bot > b_top && t_left < b_left && t_right > b_right) {
                console.log('hit');
                var score_board = document.getElementById("score_board");
                var add_points = t_bid.getAttribute("points");
                current_points = current_points + parseInt(add_points);
                score_board.innerText = current_points;
                t_bid.remove();
                for(var r = 0; r < bid.length; r++) {
                    if(bid[r] == t_bid.id) {
                        bid.splice(r, 1);
                    }
                }
            }
        }
    }
}, 10);












